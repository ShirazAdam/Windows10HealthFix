object MainForm: TMainForm
  Left = 0
  Top = 0
  AlphaBlend = True
  BorderIcons = [biMinimize]
  BorderStyle = bsSingle
  Caption = 'Windows 10 Health Fix'
  ClientHeight = 481
  ClientWidth = 954
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  PixelsPerInch = 96
  DesignSize = (
    954
    481)
  TextHeight = 13
  object SFCButton: TButton
    Left = 8
    Top = 448
    Width = 121
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'SFC /ScanNow'
    TabOrder = 0
    OnClick = SFCButtonClick
    ExplicitTop = 191
  end
  object DISMButton: TButton
    Left = 192
    Top = 448
    Width = 614
    Height = 25
    Anchors = [akLeft, akRight, akBottom]
    Caption = 'DISM /Online /Cleanup-Image /RestoreHealth'
    TabOrder = 1
    OnClick = DISMButtonClick
    ExplicitTop = 191
    ExplicitWidth = 232
  end
  object LogMemo: TMemo
    Left = 8
    Top = 8
    Width = 938
    Height = 434
    Anchors = [akLeft, akTop, akRight, akBottom]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Tahoma'
    Font.Style = []
    Lines.Strings = (
      'Welcome to Windows 10 Health Fix software (written in RAD '
      'Studio Delphi '
      '10.2 Tokyo and updated in RAD Studio Delphi 11.0 Alexandria)...')
    ParentFont = False
    TabOrder = 2
    ExplicitWidth = 556
    ExplicitHeight = 177
  end
  object CloseButton: TButton
    Left = 871
    Top = 448
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Close'
    TabOrder = 3
    OnClick = CloseButtonClick
    ExplicitLeft = 489
    ExplicitTop = 191
  end
end
